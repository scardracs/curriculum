# Curriculum Vitae di Scardovi Marco

# Informazioni Personali
- Sesso: Maschile
- Luogo e Data di Nascita: Lugo (RA), 02 Ottobre 1995
- Comune di Residenza: Russi (RA)
- Nazionalità: Italiana
- Recapito telefonico: Nascosto per motivi di privacy
- Email: Nascosto per motivi di privacy
- Per contatti: [Telegram](https://t.me/scardracs), [Twitter](https://twitter.com/scardracs)
- Patente: B

# Esperienze Professionali  
N.B: Le esperienze sono in ordine cronologico inverso: la prima è la più recente  
### CM srl (dal 15/10/2020 al 09/04/2021)  
Sede: Conselice (RA)  
Mansioni: Montaggio e riparazione di asfaltatrici

### PGM srl (dal 10/09/2018 al 31/08/2020)  
Sede: Lugo (RA)  
Mansioni:
- Redazione di documentazione tecnica, quali: Manuali Uso e Manutenzione, Schemi elettrici e pneumatici, Cataloghi Ricambi
- Creazione e Gestione siti
- Redazione di programmi interattivi in HTML5/CSS3/JavaScript
- Realizzazione e gestione di database in Excel/MySQL
- Utilizzo di programmi Office e Adobe
- Realizzazione di script avanzati per programmi Adobe
- Utilizzo di programmi per la visualizzazione 3D (ad esempio Creo Illustrate)

### Zanellato srl (dal 07/01/2018 al 06/08/2018)  
Sede: Conselice (RA)  
Mansioni:
- Controllo di torni e frese CNC a 4 assi
- Tornitura a tornio manuale
- Fresatura a CNC e manuale

### Pentatech srl (dal 05/10/2017 al 23/12/2017)  
Sede: Lavezzola (RA)  
Mansioni: Montaggio meccanico, pneumatico ed elettrico di macchine per l'imballaggio.

### VR Plast srl (dal 01/11/2016 al 30/09/2017)  
Sede: Lugo (RA)  
Mansioni: Tecnico programmatore di pantografi a 4 e 5 assi per lavorazioni in plastica

### Tellarini Pompe srl (dal 01/10/2015 al 01/09/2016)  
Sede: Lugo (RA)  
Mansioni:
- Montaggio di pompe elettrromeccaniche autoadescanti (sia meccanico che elettrico)
- Tornitura e fresatura di compnenti in C40, acciaio inox AISI 310 e 316 e ottone
- Saldatura di C40 e acciaio inox AISI 310 e 316

# Istruzione e Formazione
### Perito tecnico meccanico e meccatronico (dal 07/09/2009 al 01/07/2014)  
Sede: ITIS "G. Marconi", Lugo (RA)
- Valutazione finale: 61/100
- Materie generiche: Matematica, Italiano, Geografia, Inglese, Diritto ed economia, Educazione fisica
- Materie techniche: Meccanica, Elettronica, Pneumatica, PLC, Disegno tecnico, Tecnologia dei materiali

# Competenze Personali
- Lingua madre: Italiano
- Lingue straniere: Inglese
- Valutazione lingue straniere:
- Produzione scritta: B2
- Comprensione in ascolto: B2
- Comprensione in lettura: C1
- Parlato in interazione: B1
- Parlato in produzione orale: B1

Autovalutazione eseguita secondo il Quadro Comune Europeo di Riferimento delle Lingue - Scheda per l'autovalutazione

# Competenze digitali
- Conoscenza di Windows: avanzato
- Conoscenza di GNU/Linux: avanzato (vedi note personali)
- Elaborazione informazioni: avanzato
- Comunicazione: avanzato
- Creazione contenuti: avanzato
- Sicurezza: avanzato
- Risoluzione problemi: avanzato

### Note personali
Attualmente uso Gentoo Linux come sistema operativo principale, oltre ad esserne un manutentore. Ho intoltre fatto uso di altri OS basati su GNU/Linux, come: ArchLinux, Fedora, openSUSE, Clear Linux, Ubuntu, ma anche OS basati su BSD, quali: OpenBSD, FreeBSD e NetBSD

# Altre Competenze
- Certificato di corso per la sicurezza ad alto rischio

# Trattamento dei Dati Personali
Autorizzo il trattamento dei dati personali contenuti nel mio curriculum vitae in base all’art. 13 del D. Lgs. 196/2003 e all’art. 13 del Regolamento UE 2016/679 relativo alla protezione delle persone fisiche con riguardo al trattamento dei dati personali.
